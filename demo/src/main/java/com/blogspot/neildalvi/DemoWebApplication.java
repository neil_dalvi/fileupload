package com.blogspot.neildalvi;

import org.apache.catalina.Context;
import org.apache.catalina.Wrapper;
import org.apache.catalina.servlets.DefaultServlet;
import org.apache.catalina.startup.Tomcat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class DemoWebApplication extends SpringBootServletInitializer {

	public static String UPLOAD_FOLDER_ROOT_PATH = "";

	@Value("${static.path}")
	private static String staticPath;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DemoWebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		UPLOAD_FOLDER_ROOT_PATH = args[args.length-1];
		staticPath = UPLOAD_FOLDER_ROOT_PATH;
		 new SpringApplicationBuilder()
         .sources(DemoWebApplication.class)
         .run(args);
		//SpringApplication.run(DemoWebApplication.class, args);
	}

//	@Bean
//	public TomcatEmbeddedServletContainerFactory tomcatFactory() {
//		return new TomcatEmbeddedServletContainerFactory() {
//			@Override
//			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(Tomcat tomcat) {
//				TomcatEmbeddedServletContainer container = super.getTomcatEmbeddedServletContainer(tomcat);
//				Context context = container.getTomcat().addContext("/foobar", "D:\\UploadFolder\\temp5");
//				Wrapper defaultServlet = context.createWrapper();
//				defaultServlet.setName("default");
//				defaultServlet.setServlet(new DefaultServlet());
//				defaultServlet.addInitParameter("debug", "0");
//				defaultServlet.addInitParameter("listings", "false");
//				defaultServlet.setLoadOnStartup(1);
//				context.addChild(defaultServlet);
//				context.addServletMappingDecoded("/foobar", "default");
//				return container;
//			}
//		};
//	}

	@Bean
	public WebMvcConfigurerAdapter webMvcConfigurerAdapter() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addResourceHandlers(ResourceHandlerRegistry registry) {
				if (!registry.hasMappingForPattern("/**")) {
					// if this is executed spring won't add default resource
					// locations - add them to the staticContentLocations if
					// you want to keep them
					// default locations:
					// WebMvcAutoConfiguration.RESOURCE_LOCATIONS
					registry.addResourceHandler("/foobar/**").addResourceLocations("file:" + staticPath + "\\temp5\\");
				}
			}
		};
	}
}
