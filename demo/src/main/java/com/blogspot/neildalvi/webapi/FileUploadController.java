package com.blogspot.neildalvi.webapi;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.apache.catalina.startup.Tomcat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.blogspot.neildalvi.service.FileUploadService;
import com.blogspot.neildalvi.webapispec.fileupload.FileUploadResponse;

@RestController
public class FileUploadController {

	@Autowired
	private FileUploadService uploadService;

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody Response uploadFile(HttpServletRequest request) throws IOException, ServletException {
		FileUploadResponse response = uploadService.uploadFile(request);
		if (response != null && response.getIsError()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.status(Response.Status.ACCEPTED).build();
	}

	@RequestMapping(value = "/fetchUpload/token/{token}/filename/{fileName}/video/{type}", method = RequestMethod.GET)
	public @ResponseBody FileUploadResponse fetchOnUploadCompletetion(@PathVariable(name="token") String token, @PathVariable(name="fileName") String fileName,@PathVariable(name="type") String type)
			throws IOException, ServletException {
		return uploadService.fetchOnUploadCompletion(token, fileName, type);
	}
}
