package com.blogspot.neildalvi.service;

import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

public class CheckSumServiceImplTest {
	
	@InjectMocks
	private CheckSumService checkSumService = new CheckSumServiceImpl();

	@Test
	public void test() throws NoSuchAlgorithmException {
		Assert.assertEquals(checkSumService.validateCheckSum(new byte[]{0x11}, "123"), false);
		Assert.assertEquals(checkSumService.validateCheckSum("1234567890".getBytes(), "E807F1FCF82D132F9BB018CA6738A19F".toLowerCase()), true);
	}

}
