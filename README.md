# Setup 
Prerequisite: Java 1.8 & Windows Systems.

To start the application run following code.
java -jar fileupload-0.0.1-SNAPSHOT.war <video-folder>

<video-folder> is used to save the video uploaded.

Example:
java -jar fileupload-0.0.1-SNAPSHOT.war "D:\UploadFolder"

Starting Url.
http://localhost:8080/test.html 

# User Operatoin 
- User clicks 'Select File' to choose a file.
- The File choosen is uploaded to the server.
- Once the upload is completed, it is loaded in the player.

# Explaination 

#  Client Side  
- resumable.js is used to break the video file into chunks 0.5MB
- File is uploaded with 3 chunks simultaneously.
- It is gauranteed that each chunk size is lesser than 0.5 MB
- Upload API hits following url http://localhost:8080/upload'
- Along with the file, chunk related attributes & checksum are sent.
- Once the chunk uploading is completed another call is made to retrive the whole file 
(/fetchUpload/token/{token}/filename/{fileName}/video/{type})

#  Server Side  
- After receiving a chunk, checksum is checked and the chunk is saved.
- On receiveing the request for the whole file, the file is merged and related chunks are deleted.

# Tests 
- The application is tested for sucessive uploads for MP4 video files.
- It is tested with 500MB upload.
- The Application is tested on windows system

# Limitations 
- client side is made with HTML5 support only.
- There is no react / video js component on the client side.
- Currently, the application only supports MP4 video format.
