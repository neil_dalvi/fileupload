package com.blogspot.neildalvi.service;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.blogspot.neildalvi.service.entity.FileStorageEntity;
import com.blogspot.neildalvi.webapi.FileUploadController;
import com.blogspot.neildalvi.webapispec.fileupload.FileUploadResponse;
import com.blogspot.neildalvi.webapispec.fileupload.enums.FileUploadError;
import com.google.common.primitives.Bytes;

@Service
public class FileUploadServiceImpl implements FileUploadService {

	private static final int CHUNK_SIZE = 1024 * 512;

	@Autowired
	private CheckSumService checkSumService;
	
	@Autowired
	private FileStorageService fileStorageService;

	private final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	@Override
	public FileUploadResponse uploadFile(HttpServletRequest request) throws ServletException {
		FileUploadResponse response = null;
		try {
			FileStorageEntity storageEntity = extractFileStorageEntity(request);
			fileStorageService.saveFileChunk(storageEntity );
		} catch (IOException e) {
			logger.error(e.getMessage());
			response = new FileUploadResponse(FileUploadError.INTERNAL_SERVER_ERROR);
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.getMessage());
			response = new FileUploadResponse(FileUploadError.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	private FileStorageEntity extractFileStorageEntity(HttpServletRequest request)
			throws IOException, ServletException, NoSuchAlgorithmException {
		FileStorageEntity storageEntity = null;
		String uploadToken = request.getParameter("upload_token");
		int resumableChunkNumber= Integer.parseInt(request.getParameter("resumableChunkNumber"));
		int resumableCurrentChunkSize = Integer.parseInt(request.getParameter("resumableCurrentChunkSize"));
		String resumableFileName = request.getParameter("resumableFilename");
		String totalChunks = request.getParameter("resumableTotalChunks");
		String checksum = request.getParameter("md5sum");
		byte[] fileData = new byte[0];
		final Part filePart = request.getPart("file");
		long readed = 0;
		byte[] bytes = new byte[resumableCurrentChunkSize];
		InputStream is = filePart.getInputStream();
		if(readed < resumableCurrentChunkSize) {
		    int r = is.read(bytes);
		    fileData = Bytes.concat(fileData, bytes);
		}
		logger.debug("{} bytes read for content {} for file {} ", fileData.length, uploadToken, resumableFileName);
		
		if (!checkSumService.validateCheckSum(fileData, checksum)) {
			throw new IllegalArgumentException("check sum mismatch for chunk : " + resumableChunkNumber);
		}

		storageEntity = new FileStorageEntity(uploadToken, resumableChunkNumber, resumableCurrentChunkSize, resumableFileName, totalChunks, checksum, fileData);
		return storageEntity;
	}

//	private byte[] mergeFileBytes(List<MultipartFile> uploadFile) throws IOException {
//		byte[] allBytes = null;
//		for (MultipartFile file : uploadFile) {
//			if (file.isEmpty()) {
//				continue; // next pls
//			}
//			allBytes = Bytes.concat(allBytes, file.getBytes());
//		}
//		return allBytes;
//	}

	@Override
	public FileUploadResponse fetchOnUploadCompletion(String token, String fileName, String type) throws ServletException, IOException {
		
		String filePath = fileStorageService.joinFiles(token, fileName, type);
		return new FileUploadResponse(filePath.toString());
	}
	
}
