package com.blogspot.neildalvi.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.blogspot.neildalvi.webapispec.fileupload.FileUploadResponse;

public interface FileUploadService {

	FileUploadResponse uploadFile(HttpServletRequest request) throws ServletException;
	
	FileUploadResponse fetchOnUploadCompletion(String token, String fileName, String type) throws ServletException, IOException;
}
