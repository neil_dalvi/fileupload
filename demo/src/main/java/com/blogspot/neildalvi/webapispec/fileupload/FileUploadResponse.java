package com.blogspot.neildalvi.webapispec.fileupload;

import com.blogspot.neildalvi.webapispec.fileupload.enums.FileUploadError;

public class FileUploadResponse {

	String fileId;
	
	Boolean isError;
	
	FileUploadError uploadError;

	public FileUploadResponse(FileUploadError uploadError) {
		super();
		this.isError = true;
		this.uploadError = uploadError;
	}

	public String getFileId() {
		return fileId;
	}

	public Boolean getIsError() {
		return isError;
	}

	public FileUploadError getUploadError() {
		return uploadError;
	}

	public FileUploadResponse(String fileId) {
		super();
		this.fileId = fileId;
	}
	
}
