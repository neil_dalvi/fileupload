package com.blogspot.neildalvi.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Service;

@Service
public class CheckSumServiceImpl implements CheckSumService {

	@Override
	public boolean validateCheckSum(byte[] bytes, String checksum) throws NoSuchAlgorithmException {
		
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] digest = md.digest(bytes);
		
		if(convertByteToHex(digest).equals(checksum)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Note: Hexed characters are lower cased.
	 * @param mdbytes
	 * @return
	 */
	private String convertByteToHex(byte[] mdbytes) {
		StringBuffer sb = new StringBuffer("");
	    for (int i = 0; i < mdbytes.length; i++) {
	    	sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	    }
	    return sb.toString();
	}

}
