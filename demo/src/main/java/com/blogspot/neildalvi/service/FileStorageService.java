package com.blogspot.neildalvi.service;

import java.io.File;
import java.io.IOException;

import com.blogspot.neildalvi.service.entity.FileStorageEntity;

public interface FileStorageService {

	public void saveFileChunk(FileStorageEntity storageEntity) throws IOException;

	public String joinFiles(String token, String fileName, String type) throws IOException;
	
}
