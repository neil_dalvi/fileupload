package com.blogspot.neildalvi.service.entity;

public class FileStorageEntity {

	private String uploadToken;

	private int resumableChunkNumber;

	private int resumableCurrentChunkSize;

	private String resumableFileName;

	private String totalChunks;

	private String checksum;

	private byte[] fileData;

	public FileStorageEntity(String uploadToken, int resumableChunckNumber, int resumableCurrentChunkSize,
			String resumableFileName, String totalChunks, String checksum, byte[] fileData) {
		super();
		this.uploadToken = uploadToken;
		this.resumableChunkNumber = resumableChunckNumber;
		this.resumableCurrentChunkSize = resumableCurrentChunkSize;
		this.resumableFileName = resumableFileName;
		this.totalChunks = totalChunks;
		this.checksum = checksum;
		this.fileData = fileData;
	}

	public String getUploadToken() {
		return uploadToken;
	}

	public int getResumableChunckNumber() {
		return resumableChunkNumber;
	}

	public int getResumableCurrentChunkSize() {
		return resumableCurrentChunkSize;
	}

	public String getResumableFileName() {
		return resumableFileName;
	}

	public String getTotalChunks() {
		return totalChunks;
	}

	public String getChecksum() {
		return checksum;
	}

	public byte[] getFileData() {
		return fileData;
	}

}
