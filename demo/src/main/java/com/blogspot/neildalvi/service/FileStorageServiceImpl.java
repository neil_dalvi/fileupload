package com.blogspot.neildalvi.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.blogspot.neildalvi.DemoWebApplication;
import com.blogspot.neildalvi.service.entity.FileStorageEntity;
import com.blogspot.neildalvi.webapi.FileUploadController;

@Service
public class FileStorageServiceImpl implements FileStorageService {

	private static final String UPLOAD_TEMP_FOLDER = "\\temp5\\";

	private static final String CHUNK_NUMBER_SEPARATOR = "#";

	private static String UPLOADED_FOlDER = UPLOAD_TEMP_FOLDER;

	private final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	@Override
	public void saveFileChunk(FileStorageEntity storageEntity) throws IOException {

		StringBuilder filePath = new StringBuilder(DemoWebApplication.UPLOAD_FOLDER_ROOT_PATH);
		new File(filePath.toString()).mkdir();
		filePath.append(UPLOAD_TEMP_FOLDER);
		new File(filePath.toString()).mkdir();
		filePath.append(storageEntity.getUploadToken()).append("\\");
		File rootFolder = new File(filePath.toString());
		rootFolder.mkdir();
		// FileUtils.cleanDirectory(rootFolder);
		filePath.append(storageEntity.getResumableChunckNumber()).append(CHUNK_NUMBER_SEPARATOR)
				.append(storageEntity.getResumableFileName());
		File file = new File(filePath.toString());
		file.createNewFile();
		FileOutputStream fs = new FileOutputStream(file);
		fs.write(storageEntity.getFileData());
		fs.close();
	}

	public void saveMetaInfo(FileStorageEntity storageEntity) throws IOException {
		StringBuilder sb = new StringBuilder(DemoWebApplication.UPLOAD_FOLDER_ROOT_PATH + UPLOADED_FOlDER);
		sb.append(storageEntity.getUploadToken()).append("//").append(storageEntity.getResumableFileName()).append("-")
				.append(storageEntity.getResumableChunckNumber());
		RandomAccessFile raf = new RandomAccessFile(sb.toString(), "rw");
		raf.write(storageEntity.getFileData());
		raf.close();

	}

	@Override
	public String joinFiles(String token, String fileName, String type) throws IOException {
		OutputStream output = null;
		try {
			StringBuilder filePath = new StringBuilder(DemoWebApplication.UPLOAD_FOLDER_ROOT_PATH + UPLOADED_FOlDER);
			filePath.append(token).append("\\");
			File[] sources = new File(filePath.toString()).listFiles(new FileFilter() {

				@Override
				public boolean accept(File pathname) {
					return pathname.getName().contains(CHUNK_NUMBER_SEPARATOR);
				}
			});
			if (sources.length == 0) {
				logger.warn("no file found");
				return "";
			}
			Arrays.sort(sources, new Comparator<File>() {
				@Override
				public int compare(File o1, File o2) {
					return Integer.parseInt(o1.getName().substring(0, o1.getName().indexOf('#')))
							- Integer.parseInt(o2.getName().substring(0, o2.getName().indexOf('#')));
				}
			});
			filePath.append(fileName);

			if (type.equals("mp4")) {
				filePath.append(".mp4");
			}

			File file = new File(filePath.toString());
			file.createNewFile();
			output = createAppendableStream(file);

			for (File source : sources) {
				appendFile(output, source);
				source.delete();
			}
			return filePath.toString().substring(filePath.indexOf(UPLOAD_TEMP_FOLDER) + UPLOAD_TEMP_FOLDER.length());
		} finally {
			IOUtils.closeQuietly(output);
		}
	}

	private BufferedOutputStream createAppendableStream(File destination) throws FileNotFoundException {
		return new BufferedOutputStream(new FileOutputStream(destination, true));
	}

	private void appendFile(OutputStream output, File source) throws IOException {
		InputStream input = null;
		try {
			input = new BufferedInputStream(new FileInputStream(source));
			IOUtils.copy(input, output);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

}
