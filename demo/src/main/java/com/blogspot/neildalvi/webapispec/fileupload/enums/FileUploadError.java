package com.blogspot.neildalvi.webapispec.fileupload.enums;

public enum FileUploadError {
	
	/**
	 * check sum as calculated by MD5 mismatched.
	 */
	CHECKSUM_MISMATCH_MD5,
	
	/**
	 * check sum value is missing in request argument
	 */
	CHECKSUM_MISSING,
	
	/**
	 * file upload value is missing in the request argument
	 */
	UPLOAD_FILE_MISSING,
	
	/**
	 * insufficient storage on the server.
	 */
	INSUFFICIENT_STORAGE_SPACE,
	
	/**
	 * For an unexplained error on the server side.
	 */
	INTERNAL_SERVER_ERROR;
}
