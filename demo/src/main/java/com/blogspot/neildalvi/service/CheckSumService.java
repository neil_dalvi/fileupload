package com.blogspot.neildalvi.service;

import java.security.NoSuchAlgorithmException;

public interface CheckSumService {

	public boolean validateCheckSum(byte[] bytes, String checksum) throws NoSuchAlgorithmException;
}
